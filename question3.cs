using System;
namespace assignment1
{
    public class question3
    {
        public static void RunProgram()
        {
            int n;
            Console.Write("n = ");
            n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                double creditLimit;
                Console.Write("Credit limit:");
                creditLimit = double.Parse(Console.ReadLine());
                double price;
                Console.Write("Enter price:");
                price = double.Parse(Console.ReadLine());
                int quantity;
                do
                {
                    Console.Write("Enter quantity:");
                    quantity = int.Parse(Console.ReadLine());
                    double purchase = quantity * price;
                    if (purchase > creditLimit)
                    {
                        Console.WriteLine("Sorry you cannot purchase goods worthy such a value on credit");
                    }
                    else
                    {
                        Console.WriteLine("Thank You for purchasing from us");
                        break;
                    }
                } while (true);
            }
            Console.ReadKey();
        }
    }
}