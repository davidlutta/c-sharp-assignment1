using System;
namespace assignment1
{
    public class question2
    {
        public static void RunProgram()
        {
            Symbols();
            Console.ReadKey();
        }
        static void Symbols()
        {
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i == 0 || i == 6)
                    {
                        Console.Write("&");
                    }
                    else if (i == j)
                    {
                        Console.Write("&");
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
