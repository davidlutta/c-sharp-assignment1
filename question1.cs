using System;
namespace assignment1
{
    public class question1
    {
        public static void RunProgram(){
            const int MAX_ROUNDS = 10;
            Console.WriteLine("=========== WELCOME HUMAN ===========");
            Console.WriteLine("Rules of the Game");
            Console.WriteLine("1.Enter the number of rounds you want to play.\n2.For each round enter a lucky number. This number will be divided by a \"secret number\".\n3.You win if your total score at the end of each round is positive.");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Note: the maximum number of rounds is {0}!", MAX_ROUNDS);
            Console.ResetColor();
            Console.WriteLine("=====================================");
            Console.WriteLine("Let's start how many rounds do you want to play ?");

            int rounds = Convert.ToInt32(Console.ReadLine());

            if(rounds>1 && rounds <=MAX_ROUNDS){
                int score = 0;
                int secretNumber = new Random().Next(0,10000);

                while(rounds>0){
                    Console.Write("Give me a number: ");
                    double luckyNumber = Convert.ToDouble(Console.ReadLine());
                    double remainder = luckyNumber % secretNumber;
                    if(remainder == 0){
                        score++;
                    } else if(remainder % 2 == 0){
                        score = score+3;
                    } else if(remainder % 2 != 0){
                        score = score-3;
                    }
                    rounds--;
                }
                if(score > 0){
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("CONGRATULATIONS YOU WIN !!! Your final Score was: {0}", score);
                }else{
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Sorry you lost Your final score was: {0}", score);
                    Console.ResetColor();
                    Console.ReadKey();
                }
            }else{
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Sorry the number of rounds entered \"{0}\" was invalid try again !", rounds);
                Console.ResetColor();
                Environment.Exit(0);
            }
        }
    }
}