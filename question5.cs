using System;
namespace assignment1
{
    class question5
    {
        public static void RunProgram()
        {
            const short countSalesMen = 10;
            string[] salesmen = new string[countSalesMen];
            int[] items1 = new int[countSalesMen];
            int[] items2 = new int[countSalesMen];
            int[] items3 = new int[countSalesMen];
            int[] items4 = new int[countSalesMen];
            int[] items5 = new int[countSalesMen];
            for (int i = 0; i < countSalesMen; i++)
            {
                salesmen[i] = SetName();
                items1[i] = SetSales(1);
                items2[i] = SetSales(2);
                items3[i] = SetSales(3);
                items4[i] = SetSales(4);
                items5[i] = SetSales(5);
                Console.WriteLine();
            }
            long grandTotal = 0;
            Console.WriteLine("Name Item1 Item2 Item3 Item3 Item4 Item5 TotalSales");
            Console.WriteLine("---------------------------------------------------");
            for (int i = 0; i < countSalesMen; i++)
            {
                long totalSales = items1[i] + items2[i] + items3[i] + items4[i] + items5[i];
                grandTotal += totalSales;
                Console.WriteLine($"{salesmen[i]} {items1[i]} {items2[i]} {items3[i]} {items4[i]} {items5[i]} {totalSales}");
            }
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine($"GrandTotal {grandTotal}");
        }
        static string SetName()
        {
            Console.Write("Enter the salesmen name: ");
            return Console.ReadLine();
        }
        static int SetSales(short itemNumber)
        {
            int sales = 0;
            bool error = true;
            do
            {
                Console.Write($"Enter the sales made of the item{itemNumber}: ");
                try
                {
                    sales = Convert.ToInt32(Console.ReadLine());
                    error = false;
                }
                catch (Exception)
                {
                    Console.WriteLine("Input error. Please enter an integer.");
                }
            } while (error); return sales;
        }
    }
}
