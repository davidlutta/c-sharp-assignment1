﻿using System;
/*
A program is required for a computer game. 
The user keys in the number of rounds he wishes to play.
For each round the user enters his lucky number. The program takes the number and divides it with a secret number.
If the remainder of the division is zero, it is considered a draw for the round and the total score is incriminated by 1.
Otherwise if it is any other even number, it is considered a win for the round and the total score is incremented by 3.
However if it is an odd number, it is considered a loss for the round and the total score is decremented by 3. 
This is done until he completes his rounds. He wins if the total score at the end is a positive number.
*/
namespace assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            question4.RunProgram();
            // question1.RunProgram();
            // question2.RunProgram();
            // question5.RunProgram();
            // question3.RunProgram();
        }
    }
}
