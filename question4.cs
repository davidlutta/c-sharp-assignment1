using System;
namespace assignment1
{
    public class question4
    {
        public static void RunProgram()
        {
            Console.WriteLine("Welcome to Area Calculator\nYou can calculate the area of the following shapes:\n1. Rectangle\n2. Triangle\n3. Circle\n");
            Console.Write("Enter the name of the shape you want me to calculate the area of: ");
            string response = Console.ReadLine();
            response.Trim();
            response.ToLower();
            if(response.Equals("triangle")){
                Triangle();
            } else if (response.Equals("rectangle")){
                Rectangle();
            }else if (response.Equals("circle")){
                Circle();
            }else {
                Console.WriteLine("Invalid Option");
                Environment.Exit(0);
            }
            Console.ReadKey();
        }

        static void Triangle()
        {
            Console.WriteLine("Enter the base for Triangle ");
            float basefortriangle = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter the Height for Triangle ");
            float height = float.Parse(Console.ReadLine());
            Console.WriteLine("Area of Triangle is:{0}", ((basefortriangle * height) / 2));
        }

        static void Rectangle()
        {
            Console.WriteLine("Enter the Length for Rectangle");
            float length = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter the breadth for Rectangle");
            float breadth = float.Parse(Console.ReadLine());
            Console.WriteLine("Area of rectangle is :{0}", (length * breadth));
        }

        static void Circle()
        {
            Console.WriteLine("Enter the Radius of the Circle");
            float radius = float.Parse(Console.ReadLine());
            Console.WriteLine("Area of Circle is:{0}", (3.14 * radius * radius));
        }

    }
}